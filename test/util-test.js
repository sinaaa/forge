import test from 'tape';
import u from '../src/util';

test('stringToBoxShadow', (t) => {
  const b = u.stringToBoxShadow(' 1px 2px 3px #eff');
  t.deepEqual(b, {
    x: 1,
    y: 2,
    blur: 3,
    spread: 0,
    color: '#eff',
    inset: false,
  });
  t.end();
});

test('stringToBoxShadow inset', (t) => {
  const b = u.stringToBoxShadow(' inset  0 0 0 0 rgb(0,0,0)  ');
  t.deepEqual(b, {
    x: 0,
    y: 0,
    blur: 0,
    spread: 0,
    color: 'rgb(0,0,0)',
    inset: true,
  });
  t.end();
});
