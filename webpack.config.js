const path = require('path');
const autoprefixer = require('autoprefixer');

const PROD = process.env.NODE_ENV === 'production';

module.exports = {
  devtool: PROD ? 'source-map' : 'eval',

  entry: {
    app: PROD ? './src/index' : [
      'babel-polyfill',
      'react-hot-loader/patch',
      './src/index',
    ],
  },

  output: {
    path: path.resolve(__dirname, 'public'),
    filename: '[name].js',
  },

  resolve: {
    extensions: ['.js', '.jsx'],
  },

  devServer: {
    contentBase: path.resolve(__dirname, 'public'),
    publicPath: '/',
  },

  module: {
    rules: [
      {
        test: /\.jsx?$/,
        include: [
          path.resolve(__dirname, 'src'),
        ],
        use: [{
          loader: 'babel-loader',
          options: {
            presets: ['stage-3', ['es2015', { modules: false }], 'react'],
            plugins: ['react-hot-loader/babel'],
          },
        }],
      },
      {
        test: /\.css$/,
        use: [
          'style-loader',
          {
            loader: 'css-loader',
            options: { importLoaders: 1 },
          },
          {
            loader: 'postcss-loader',
            options: {
              ident: 'postcss', // https://webpack.js.org/guides/migrating/#complex-options
              plugins: () => [
                autoprefixer({
                  browsers: [
                    '>1%',
                    'last 4 versions',
                    'Firefox ESR',
                    'not ie < 9', // React doesn't support IE8 anyway
                  ],
                }),
              ],
            },
          },
        ],
      },
    ],
  },
};
