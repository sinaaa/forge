module.exports = {
  parser: 'babel-eslint',
  extends: [
    'airbnb',
  ],
  globals: {
    window: false,
    document: false,
  },
  rules: {
    'react/forbid-prop-types': 'off',
    'react/prefer-stateless-function': 'off',
    'jsx-a11y/no-static-element-interactions': 'off',
    'react/prop-types': ['off', { skipUndeclared: true }],
    'react/jsx-no-bind': 'off',
    'no-shadow': 'off',
    'consistent-return': 'off',
    'default-case': 'off',
  },
};
