export default {
  clamp(value, min, max) {
    if (value < min) return min;
    if (value > max) return max;
    return value;
  },

  debounce(fn, interval) {
    let timeoutId;
    let result;

    const debounced = (...args) => {
      const ctx = this;
      clearTimeout(timeoutId);

      timeoutId = setTimeout(() => {
        timeoutId = null;
        result = fn.apply(ctx, args);
      }, interval);

      return result;
    };

    debounced.cancel = () => clearTimeout(timeoutId);
    return debounced;
  },

  pick(obj, keys) {
    return Object.keys(obj).reduce((acc, key) => {
      if (!keys.includes(key)) return acc;
      return { ...acc, [key]: obj[key] };
    }, {});
  },

  shallowEquals(o1, o2) {
    return Object.keys(o1).length === Object.keys(o2).length
      && Object.keys(o1).every(key => o1[key] === o2[key]);
  },

  stringToBoxShadow(string) {
    const re = /(^|\s)(0|-?[0-9]+px)(\s+(0|-?[0-9]+px)){1,3}(\s|$)/;
    const matches = string.match(re);
    let vals = [];
    let color = 'rgba(0,0,0,.2)';
    if (matches) {
      vals = matches[0].trim().split(' ').map(v => parseInt(v, 10));
      color = string.replace(matches[0], '').replace('inset', '').trim();
    }
    return {
      x: vals[0] || 0,
      y: vals[1] || 0,
      blur: vals[2] || 0,
      spread: vals[3] || 0,
      color,
      inset: /inset/.test(string),
    };
  },

  boxShadowToString({ x, y, blur, spread, color, inset }) {
    let str = `${x}px ${y}px ${blur}px ${spread}px ${color}`;
    if (inset) str += ' inset';
    return str;
  },
};
