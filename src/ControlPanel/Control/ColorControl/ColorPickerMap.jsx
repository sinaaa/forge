import React from 'react';
import PropTypes from 'prop-types';
import u from '../../../util';

export default class ColorPickerMap extends React.Component {
  constructor(props) {
    super(props);
    this.handleMouseDown = this.handleMouseDown.bind(this);
    this.handleMouseUp = this.handleMouseUp.bind(this);
    this.handleMouseMove = this.handleMouseMove.bind(this);
  }

  componentWillUnmount() {
    this.removeEventListeners();
  }

  addEventListeners() {
    document.addEventListener('mousemove', this.handleMouseMove);
    document.addEventListener('mouseup', this.handleMouseUp);
  }

  removeEventListeners() {
    document.removeEventListener('mousemove', this.handleMouseMove);
    document.removeEventListener('mouseup', this.handleMouseUp);
  }

  handleMouseDown(e) {
    if (e.button !== 0) {
      return;
    }
    e.preventDefault();
    this.updateHsv(e);
    this.addEventListeners();
  }

  handleMouseUp(e) {
    if (e.button !== 0) {
      return;
    }
    e.preventDefault();
    this.removeEventListeners();
  }

  handleMouseMove(e) {
    this.updateHsv(e);
  }

  updateHsv(e) {
    const rect = this.el.getBoundingClientRect();
    const x = u.clamp(e.clientX - rect.left, 0, 219);
    const y = u.clamp(e.clientY - rect.top, 0, 119);
    const s = x / 219;
    const v = 1 - (y / 119);
    this.props.onChange({ s, v });
  }

  render() {
    const { h, s, v } = this.props;
    const x = Math.round(s * 219);
    const y = Math.round((1 - v) * 119);
    const knobStyle = { transform: `translate(${x}px, ${y}px)` };
    const mapStyle = {
      backgroundImage: `linear-gradient(0deg, #000, rgba(0,0,0,0)), linear-gradient(90deg, #fff, hsl(${h},100%,50%))`,
    };
    return (
      <div
        ref={(el) => { this.el = el; }}
        className="color-map"
        style={mapStyle}
        onMouseDown={this.handleMouseDown}
      >
        <div className="color-map__knob" style={knobStyle} />
      </div>
    );
  }
}

ColorPickerMap.propTypes = {
  h: PropTypes.number.isRequired,
  s: PropTypes.number.isRequired,
  v: PropTypes.number.isRequired,
  onChange: PropTypes.func.isRequired,
};
