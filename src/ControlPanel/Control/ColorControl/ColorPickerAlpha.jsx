import React from 'react';
import PropTypes from 'prop-types';
import u from '../../../util';

export default class ColorPickerAlpha extends React.Component {
  constructor(props) {
    super(props);
    this.handleMouseDown = this.handleMouseDown.bind(this);
    this.handleMouseUp = this.handleMouseUp.bind(this);
    this.handleMouseMove = this.handleMouseMove.bind(this);
  }

  componentWillUnmount() {
    this.removeEventListeners();
  }

  addEventListeners() {
    document.addEventListener('mousemove', this.handleMouseMove);
    document.addEventListener('mouseup', this.handleMouseUp);
  }

  removeEventListeners() {
    document.removeEventListener('mousemove', this.handleMouseMove);
    document.removeEventListener('mouseup', this.handleMouseUp);
  }

  handleMouseDown(e) {
    if (e.button !== 0) {
      return;
    }
    e.preventDefault();
    this.updateAlpha(e);
    this.addEventListeners();
  }

  handleMouseUp(e) {
    if (e.button !== 0) {
      return;
    }
    e.preventDefault();
    this.removeEventListeners();
  }

  handleMouseMove(e) {
    this.updateAlpha(e);
  }

  updateAlpha(e) {
    const rect = this.el.getBoundingClientRect();
    const a = u.clamp(e.clientX - rect.left, 0, 219);
    this.props.onChange(a / 219);
  }

  render() {
    const { a, hslString } = this.props;
    const x = a * 219;
    const knobStyle = { transform: `translateX(${x}px)` };
    const barStyle = { backgroundImage: `linear-gradient(90deg, rgba(0,0,0,0), ${hslString})` };
    return (
      <div
        ref={(el) => { this.el = el; }}
        className="color-slider--alpha"
        onMouseDown={this.handleMouseDown}
      >
        <span style={barStyle} />
        <div className="color-slider__knob" style={knobStyle} />
      </div>
    );
  }
}

ColorPickerAlpha.propTypes = {
  a: PropTypes.number.isRequired,
  hslString: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
};
