import React from 'react';
import PropTypes from 'prop-types';
import tinycolor from 'tinycolor2';
import ColorPickerMap from './ColorPickerMap';
import ColorPickerHue from './ColorPickerHue';
import ColorPickerAlpha from './ColorPickerAlpha';
import ColorInput from './ColorInput';
import './ColorPicker.css';

export default class ColorPicker extends React.Component {
  constructor(props) {
    super(props);
    this.callOnChange = this.callOnChange.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
    const { h, s, v, a } = tinycolor(props.color).toHsv();
    this.state = { h, s, v, a };
  }

  callOnChange() {
    const hsl = tinycolor(this.state).toHslString();
    this.props.onChange(hsl);
  }

  handleInputChange(string) {
    const color = tinycolor(string);
    if (!color.isValid()) {
      return;
    }
    this.setState(color.toHsv(), this.callOnChange);
  }

  render() {
    return (
      <div>
        <ColorInput
          hex={tinycolor(this.state).toHexString()}
          onChange={this.handleInputChange}
        />
        <ColorPickerMap
          h={this.state.h}
          s={this.state.s}
          v={this.state.v}
          onChange={({ s, v }) => this.setState({ s, v }, this.callOnChange)}
        />
        <ColorPickerHue
          h={this.state.h}
          onChange={h => this.setState({ h }, this.callOnChange)}
        />
        <ColorPickerAlpha
          a={this.state.a}
          hslString={tinycolor(this.state).setAlpha(1).toHslString()}
          onChange={a => this.setState({ a }, this.callOnChange)}
        />
      </div>
    );
  }
}

ColorPicker.propTypes = {
  color: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
};
