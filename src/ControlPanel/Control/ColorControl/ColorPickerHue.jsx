import React from 'react';
import PropTypes from 'prop-types';
import u from '../../../util';

export default class ColorPickerHue extends React.Component {
  constructor(props) {
    super(props);
    this.handleMouseDown = this.handleMouseDown.bind(this);
    this.handleMouseUp = this.handleMouseUp.bind(this);
    this.handleMouseMove = this.handleMouseMove.bind(this);
  }

  componentWillUnmount() {
    this.removeEventListeners();
  }

  addEventListeners() {
    document.addEventListener('mousemove', this.handleMouseMove);
    document.addEventListener('mouseup', this.handleMouseUp);
  }

  removeEventListeners() {
    document.removeEventListener('mousemove', this.handleMouseMove);
    document.removeEventListener('mouseup', this.handleMouseUp);
  }

  handleMouseDown(e) {
    if (e.button !== 0) {
      return;
    }
    e.preventDefault();
    this.updateHue(e);
    this.addEventListeners();
  }

  handleMouseUp(e) {
    if (e.button !== 0) {
      return;
    }
    e.preventDefault();
    this.removeEventListeners();
  }

  handleMouseMove(e) {
    this.updateHue(e);
  }

  updateHue(e) {
    const rect = this.el.getBoundingClientRect();
    const h = u.clamp(e.clientX - rect.left, 0, 219);
    const hue = Math.round((h / 219) * 360);
    this.props.onChange(hue);
  }

  render() {
    const x = (this.props.h / 360) * 219;
    const knobStyle = { transform: `translateX(${x}px)` };
    return (
      <div
        ref={(el) => { this.el = el; }}
        className="color-slider--hue"
        onMouseDown={this.handleMouseDown}
      >
        <div className="color-slider__knob" style={knobStyle} />
      </div>
    );
  }
}

ColorPickerHue.propTypes = {
  h: PropTypes.number.isRequired,
  onChange: PropTypes.func.isRequired,
};
