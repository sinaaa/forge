import React from 'react';
import PropTypes from 'prop-types';
import ValueInput from '../../../atoms/ValueInput/ValueInput';

export default class ColorInput extends React.Component {
  render() {
    const { onChange, hex } = this.props;
    return (
      <div className="color-input">
        <label htmlFor="hex">
          {'HEX'}
          <ValueInput
            id="hex"
            value={hex}
            onChange={onChange}
          />
        </label>
      </div>
    );
  }
}

ColorInput.propTypes = {
  hex: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
};
