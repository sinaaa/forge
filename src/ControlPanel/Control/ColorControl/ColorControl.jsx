import React from 'react';
import PropTypes from 'prop-types';
import Dropdown from '../Dropdown/Dropdown';
import ColorPicker from './ColorPicker';
import './ColorControl.css';

export default class ColorControl extends React.Component {
  shouldComponentUpdate(nextProps) {
    return nextProps.color !== this.props.color;
  }

  render() {
    const { onChange, color, title } = this.props;
    const menu = (
      <div role="menu" className="color-control__menu">
        <ColorPicker color={color} onChange={color => onChange(color)} />
      </div>
    );
    return (
      <Dropdown overlay={menu}>
        <div className="toolbar-box">
          <div>
            <div className="color-control__icon">
              <div style={{ backgroundColor: color }} />
            </div>
          </div>
          <span>{title}</span>
        </div>
      </Dropdown>
    );
  }
}

ColorControl.propTypes = {
  onChange: PropTypes.func.isRequired,
  color: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
};
