import React from 'react';
import PropTypes from 'prop-types';
import Dropdown from './Dropdown/Dropdown';
import Menu from './Dropdown/Menu';

const BorderRadiusIcon = () => (
  <svg width="8" height="8" viewBox="0 0 8 8" xmlns="http://www.w3.org/2000/svg">
    <path d="M7 .5C3 .5.5 3 .5 7" fill="none" stroke="#000" strokeOpacity=".8" strokeLinecap="round" />
  </svg>
);

export default class RadiusControl extends React.Component {
  shouldComponentUpdate(nextProps) {
    return nextProps.radius !== this.props.radius;
  }

  render() {
    const { radius, onChange } = this.props;
    return (
      <Dropdown
        overlay={<Menu
          items={[0, 1, 2, 4, 6, 8, 10, 16, 20, 40, 80, '50%']}
          selected={radius}
          onChange={borderRadius => onChange({ borderRadius })}
        />}
      >
        <div className="toolbar-box">
          <div>
            <BorderRadiusIcon />
            <p>{radius}</p>
          </div>
          <span>Radius</span>
        </div>
      </Dropdown>
    );
  }
}

RadiusControl.propTypes = {
  radius: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
  onChange: PropTypes.func.isRequired,
};
