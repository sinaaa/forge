import React from 'react';
import PropTypes from 'prop-types';
import Dropdown from './Dropdown/Dropdown';
import Menu from './Dropdown/Menu';

export default class FontSizeControl extends React.Component {
  shouldComponentUpdate(nextProps) {
    return nextProps.size !== this.props.size;
  }

  render() {
    const { size, onChange } = this.props;
    return (
      <Dropdown
        overlay={<Menu
          items={[12, 14, 16, 18, 20, 24, 28, 32]}
          selected={size}
          onChange={fontSize => onChange({ fontSize })}
        />}
      >
        <div className="toolbar-box">
          <div>
            <p>{size}</p>
          </div>
          <span>Font size</span>
        </div>
      </Dropdown>
    );
  }
}

FontSizeControl.propTypes = {
  size: PropTypes.number.isRequired,
  onChange: PropTypes.func.isRequired,
};
