import React from 'react';
import PropTypes from 'prop-types';
import u from '../../../util';
import './ShadowPreview.css';

export default class ShadowPreview extends React.Component {
  constructor(props) {
    super(props);
    this.handleMouseDown = this.handleMouseDown.bind(this);
    this.handleMouseMove = this.handleMouseMove.bind(this);
    this.handleMouseUp = this.handleMouseUp.bind(this);
    this.state = { x: props.x, y: props.y, dragging: false };
  }

  componentWillReceiveProps(nextProps) {
    const { x, y } = nextProps;
    if (x !== this.state.x || y !== this.state.y) {
      this.setState({ x, y });
    }
  }

  componentWillUnmount() {
    this.removeEventListeners();
  }

  removeEventListeners() {
    document.removeEventListener('mouseup', this.handleMouseUp);
    document.removeEventListener('mousemove', this.handleMouseMove);
  }

  handleMouseDown(e) {
    e.preventDefault();
    this.initialPos = {
      clientX: e.clientX,
      clientY: e.clientY,
      x: this.state.x,
      y: this.state.y,
    };
    this.setState({ dragging: true }, () => {
      document.addEventListener('mouseup', this.handleMouseUp);
      document.addEventListener('mousemove', this.handleMouseMove);
    });
  }

  handleMouseMove(e) {
    const dx = e.clientX - this.initialPos.clientX;
    const dy = e.clientY - this.initialPos.clientY;
    this.setState({
      x: this.initialPos.x + dx,
      y: this.initialPos.y + dy,
    }, () => this.props.onChange({ x: this.state.x, y: this.state.y }));
  }

  handleMouseUp() {
    this.setState({ dragging: false });
    this.removeEventListeners();
  }

  render() {
    const { x, y, dragging } = this.state;
    const { blur, spread, inset } = this.props;
    const boxShadow = u.boxShadowToString({ x, y, blur, spread, inset, color: 'rgba(0,0,0,.2)' });
    return (
      <div className={dragging ? 'shadow-preview--dragging' : 'shadow-preview'}>
        <div
          className="shadow-preview__box"
          style={{ boxShadow }}
        />
        <div
          className="shadow-preview__shadow"
          style={{ transform: `translate(${x}px, ${y}px)` }}
          onMouseDown={this.handleMouseDown}
        />
      </div>
    );
  }
}

ShadowPreview.propTypes = {
  x: PropTypes.number.isRequired,
  y: PropTypes.number.isRequired,
  blur: PropTypes.number.isRequired,
  spread: PropTypes.number.isRequired,
  inset: PropTypes.bool.isRequired,
  onChange: PropTypes.func.isRequired,
};
