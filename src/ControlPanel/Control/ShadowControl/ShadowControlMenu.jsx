import React from 'react';
import PropTypes from 'prop-types';
import ShadowPreview from './ShadowPreview';
import ValueInput from '../../../atoms/ValueInput/ValueInput';
import Checkbox from '../../../atoms/Checkbox/Checkbox';
import Slider from '../../../atoms/Slider/Slider';
import ColorControl from '../ColorControl/ColorControl';
import u from '../../../util';
import './ShadowControlMenu.css';

export default class ShadowControlMenu extends React.Component {
  constructor(props) {
    super(props);
    this.callOnChange = this.callOnChange.bind(this);
    const { x, y, blur, spread, color, inset } = u.stringToBoxShadow(props.boxShadow);
    this.state = { x, y, blur, spread, color, inset };
  }

  callOnChange() {
    const boxShadow = u.boxShadowToString(this.state);
    this.props.onChange({ boxShadow });
  }

  render() {
    const { x, y, blur, spread, color, inset } = this.state;
    return (
      <div role="menu" className="shadow-control-menu">
        <div className="shadow-control-menu__top">
          <label htmlFor="x">X</label>
          <ValueInput
            id="x"
            value={x}
            onChange={x => this.setState({ x }, this.callOnChange)}
            min={-100}
            max={100}
          />
          <label htmlFor="y">Y</label>
          <ValueInput
            id="y"
            value={y}
            onChange={y => this.setState({ y }, this.callOnChange)}
            min={-100}
            max={100}
          />
          <label htmlFor="inset">Inset</label>
          <Checkbox
            id="inset"
            checked={inset}
            onChange={inset => this.setState({ inset }, this.callOnChange)}
          />
        </div>
        <ShadowPreview
          x={x}
          y={y}
          blur={blur}
          spread={spread}
          inset={inset}
          onChange={({ x, y }) => this.setState({ x, y }, this.callOnChange)}
        />
        <div className="shadow-control-menu__blur-spread">
          <div>
            <label htmlFor="blur">Blur</label>
            <ValueInput
              id="blur"
              value={blur}
              onChange={blur => this.setState({ blur }, this.callOnChange)}
              min={0}
              max={20}
            />
            <Slider
              value={blur}
              onChange={blur => this.setState({ blur }, this.callOnChange)}
              min={0}
              max={20}
            />
          </div>
          <div>
            <label htmlFor="spread">Spread</label>
            <ValueInput
              id="spread"
              value={spread}
              onChange={spread => this.setState({ spread }, this.callOnChange)}
              min={0}
              max={20}
            />
            <Slider
              value={spread}
              onChange={spread => this.setState({ spread }, this.callOnChange)}
              min={0}
              max={20}
            />
          </div>
        </div>
        <ColorControl
          title="Color"
          color={color.toString()}
          onChange={color => this.setState({ color }, this.callOnChange)}
        />
      </div>
    );
  }
}

ShadowControlMenu.propTypes = {
  boxShadow: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
};
