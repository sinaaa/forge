import React from 'react';
import PropTypes from 'prop-types';
import Dropdown from '../Dropdown/Dropdown';
import ShadowControlMenu from './ShadowControlMenu';
import ShadowIcon from './ShadowIcon';

export default class ShadowControl extends React.Component {
  shouldComponentUpdate(nextProps) {
    return nextProps.boxShadow !== this.props.boxShadow;
  }

  render() {
    const { onChange, boxShadow } = this.props;
    return (
      <Dropdown overlay={<ShadowControlMenu boxShadow={boxShadow} onChange={onChange} />}>
        <div className="toolbar-box">
          <div>
            <ShadowIcon boxShadow={boxShadow} />
          </div>
          <span>Shadow</span>
        </div>
      </Dropdown>
    );
  }
}

ShadowControl.propTypes = {
  boxShadow: PropTypes.string,
  onChange: PropTypes.func.isRequired,
};

ShadowControl.defaultProps = {
  boxShadow: '',
};
