import React from 'react';
import PropTypes from 'prop-types';
import './ShadowIcon.css';

export default class ShadowIcon extends React.Component {
  render() {
    const { boxShadow } = this.props;
    return (
      <i className="shadow-icon">
        <span className="shadow-icon__preview" style={{ boxShadow }} />
      </i>
    );
  }
}

ShadowIcon.propTypes = {
  boxShadow: PropTypes.string.isRequired,
};
