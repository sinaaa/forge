import React from 'react';
import PropTypes from 'prop-types';
import './Dropdown.css';

export default class Dropdown extends React.Component {
  constructor(props) {
    super(props);
    this.handleMouseOver = this.handleMouseOver.bind(this);
    this.handleMouseOut = this.handleMouseOut.bind(this);
    this.state = {
      showOverlay: false,
    };
  }

  handleMouseOver() {
    this.setState({ showOverlay: true });
  }

  handleMouseOut(e) {
    if (!this.overlayEl.contains(e.relatedTarget) && !this.props.persist) {
      this.setState({ showOverlay: false });
    }
  }

  render() {
    return (
      <div
        className="dropdown"
        tabIndex={-1}
        onMouseOver={this.handleMouseOver}
        onMouseOut={this.handleMouseOut}
        role="menuitem"
        aria-haspopup
      >
        {this.props.children}
        {this.state.showOverlay &&
          <div
            className="overlay"
            onMouseOut={this.handleMouseOut}
            ref={(el) => { this.overlayEl = el; }}
          >
            {this.props.overlay}
          </div>
        }
      </div>
    );
  }
}

Dropdown.propTypes = {
  children: PropTypes.any.isRequired,
  overlay: PropTypes.node.isRequired,
  persist: PropTypes.bool,
};

Dropdown.defaultProps = {
  persist: false,
};
