import React from 'react';
import PropTypes from 'prop-types';
import './Menu.css';

export default class Menu extends React.Component {
  render() {
    return (
      <ul role="menu" className="menu">
        {this.props.items.map((item) => {
          let display;
          let key;

          if (Array.isArray(item)) {
            key = item[0];
            display = item[1];
          } else {
            key = item;
            display = item;
          }
          return (
            <li
              role="menuitem"
              className={`menu-item${key === this.props.selected ? ' menu-item-selected' : ''}`}
              onClick={() => this.props.onChange(key)}
              key={key}
            >
              {display}
            </li>
          );
        })}
      </ul>
    );
  }
}

Menu.propTypes = {
  items: PropTypes.array.isRequired,
  selected: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  onChange: PropTypes.func.isRequired,
};

Menu.defaultProps = {
  selected: null,
};
