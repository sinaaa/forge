import React from 'react';
import PropTypes from 'prop-types';
import Dropdown from '../Dropdown/Dropdown';
import Menu from '../Dropdown/Menu';
import './BorderStyleControl.css';

const SolidIcon = () => <div className="border-style__solid" />;
const DashedIcon = () => <div className="border-style__dashed" />;
const DottedIcon = () => <div className="border-style__dotted" />;
const DoubleIcon = () => <div className="border-style__double" />;

const renderStyleIcon = (style) => {
  switch (style) {
    case 'solid': return <SolidIcon />;
    case 'dashed': return <DashedIcon />;
    case 'dotted': return <DottedIcon />;
    case 'double': return <DoubleIcon />;
  }
};

export default class BorderStyleControl extends React.Component {
  render() {
    const { borderStyle, onChange } = this.props;
    return (
      <Dropdown
        overlay={<Menu
          items={[
            ['solid', <SolidIcon />],
            ['dashed', <DashedIcon />],
            ['dotted', <DottedIcon />],
            ['double', <DoubleIcon />],
          ]}
          selected={borderStyle}
          onChange={onChange}
        />}
      >
        <div className="border-control__box">
          {renderStyleIcon(borderStyle)}
        </div>
      </Dropdown>
    );
  }
}

BorderStyleControl.propTypes = {
  borderStyle: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
};
