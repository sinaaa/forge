import React from 'react';
import PropTypes from 'prop-types';
import Dropdown from '../Dropdown/Dropdown';
import ColorPicker from '../ColorControl/ColorPicker';

export default class BorderColorControl extends React.Component {
  render() {
    const menu = (
      <div role="menu" className="color-control__menu">
        <ColorPicker color={this.props.borderColor} onChange={this.props.onChange} />
      </div>
    );
    return (
      <Dropdown overlay={menu}>
        <div className="border-control__box">
          <div className="color-control__icon">
            <div style={{ backgroundColor: this.props.borderColor }} />
          </div>
        </div>
      </Dropdown>
    );
  }
}

BorderColorControl.propTypes = {
  onChange: PropTypes.func.isRequired,
  borderColor: PropTypes.string.isRequired,
};
