import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import './BorderIcon.css';

export default class BorderIcon extends React.Component {
  render() {
    const { style } = this.props;

    const isEmpty = !style.borderBottomWidth
      && !style.borderTopWidth
      && !style.borderLeftWidth
      && !style.borderRightWidth;

    const iconClasses = cx('border-icon', {
      'border-icon--empty': isEmpty,
    });
    const previewClasses = cx('border-icon__preview', {
      'border-icon__preview--empty': isEmpty,
    });
    const previewStyle = {
      ...style,
      marginTop: -1 * style.borderTopWidth,
      marginBottom: -1 * style.borderBottomWidth,
      marginLeft: -1 * style.borderLeftWidth,
      marginRight: -1 * style.borderRightWidth,
    };
    return (
      <i className={iconClasses}>
        <span className={previewClasses} style={previewStyle} />
      </i>
    );
  }
}

BorderIcon.propTypes = {
  style: PropTypes.object.isRequired,
};
