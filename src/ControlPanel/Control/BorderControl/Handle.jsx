import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import u from '../../../util';
import './Handle.css';

export default class Handle extends React.Component {
  constructor(props) {
    super(props);
    this.handleMouseDown = this.handleMouseDown.bind(this);
    this.handleMouseMove = this.handleMouseMove.bind(this);
    this.handleMouseUp = this.handleMouseUp.bind(this);
    this.state = {
      start: null,
      dragging: false,
    };
  }

  componentWillUnmount() {
    this.removeEventListeners();
  }

  removeEventListeners() {
    document.removeEventListener('mousemove', this.handleMouseMove);
    document.removeEventListener('mouseup', this.handleMouseUp);
  }

  handleMouseDown(e) {
    e.preventDefault();
    const { position } = this.props;
    const start = (position === 'top' || position === 'bottom')
      ? e.clientY
      : e.clientX;
    this.initialBorderWidth = this.props.borderWidth;
    this.setState({ start, dragging: true }, () => {
      document.addEventListener('mousemove', this.handleMouseMove);
      document.addEventListener('mouseup', this.handleMouseUp);
    });
  }

  handleMouseMove(e) {
    const delta = this.calcDelta(e);
    const width = delta + this.initialBorderWidth;
    this.props.onChange(u.clamp(width, 0, 20));
  }

  handleMouseUp() {
    this.initialBorderWidth = null;
    this.setState({ start: null, dragging: false });
    this.removeEventListeners();
  }

  calcDelta(e) {
    switch (this.props.position) {
      case 'left': return e.clientX - this.state.start;
      case 'right': return this.state.start - e.clientX;
      case 'top': return e.clientY - this.state.start;
      case 'bottom': return this.state.start - e.clientY;
    }
  }

  render() {
    const { borderWidth, position } = this.props;
    const style = (position === 'top' || position === 'bottom')
      ? { height: borderWidth }
      : { width: borderWidth };

    const classes = cx('handle', `handle__${position}`, {
      'handle--hover': this.state.dragging,
    });

    return (
      <div className={classes} style={style} onMouseDown={this.handleMouseDown} />
    );
  }
}

Handle.propTypes = {
  onChange: PropTypes.func.isRequired,
  borderWidth: PropTypes.number.isRequired,
  position: PropTypes.oneOf(['top', 'left', 'bottom', 'right']).isRequired,
};
