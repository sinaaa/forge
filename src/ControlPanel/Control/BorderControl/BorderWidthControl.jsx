import React from 'react';
import PropTypes from 'prop-types';
import ValueInput from '../../../atoms/ValueInput/ValueInput';
import Handle from './Handle';
import './BorderWidthControl.css';

const LockIcon = () => (
  <svg width="14px" height="14px" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg">
    <path d="M376 186h-20v-40c0-55-45-100-100-100S156 91 156 146v40h-20c-22.002 0-40 17.998-40 40v200c0 22.002 17.998 40 40 40h240c22.002 0 40-17.998 40-40V226c0-22.002-17.998-40-40-40zM256 368c-22.002 0-40-17.998-40-40s17.998-40 40-40 40 17.998 40 40-17.998 40-40 40zm62.002-182H193.998v-40c0-34.004 28.003-62.002 62.002-62.002 34.004 0 62.002 27.998 62.002 62.002v40z" />
  </svg>
);

export default class BorderWidthControl extends React.Component {
  constructor(props) {
    super(props);
    this.handleClickLock = this.handleClickLock.bind(this);
    this.onChangeBorderTopWidth = this.handleChange.bind(this, 'borderTopWidth');
    this.onChangeBorderLeftWidth = this.handleChange.bind(this, 'borderLeftWidth');
    this.onChangeBorderRightWidth = this.handleChange.bind(this, 'borderRightWidth');
    this.onChangeBorderBottomWidth = this.handleChange.bind(this, 'borderBottomWidth');
    this.state = { locked: false };
  }

  handleClickLock() {
    if (!this.state.locked) {
      this.changeAll(this.calcMinWidth());
    }
    this.setState({ locked: !this.state.locked });
  }

  handleChange(property, width) {
    if (this.state.locked) {
      this.changeAll(width);
    } else {
      this.props.onChange({ [property]: width });
    }
  }

  calcMinWidth() {
    const ps = ['borderLeftWidth', 'borderRightWidth', 'borderTopWidth', 'borderBottomWidth'];
    const widths = ps.map(p => this.props.styles[p]);
    return Math.max(...widths);
  }

  changeAll(width) {
    this.props.onChange({
      borderTopWidth: width,
      borderLeftWidth: width,
      borderRightWidth: width,
      borderBottomWidth: width,
    });
  }

  render() {
    const { styles } = this.props;
    const iconClass = this.state.locked
      ? 'border-width-control__lock-icon'
      : 'border-width-control__lock-icon--disabled';

    return (
      <div className="border-width-control">
        <div className="border-width-control__border" style={styles} />
        <i className={iconClass} onClick={this.handleClickLock}>
          <LockIcon />
        </i>
        <div className="border-width-control__top">
          <ValueInput value={styles.borderTopWidth} onChange={this.onChangeBorderTopWidth} />
          <Handle position="top" borderWidth={styles.borderTopWidth} onChange={this.onChangeBorderTopWidth} />
        </div>
        <div className="border-width-control__left">
          <ValueInput value={styles.borderLeftWidth} onChange={this.onChangeBorderLeftWidth} />
          <Handle position="left" borderWidth={styles.borderLeftWidth} onChange={this.onChangeBorderLeftWidth} />
        </div>
        <div className="border-width-control__right">
          <ValueInput value={styles.borderRightWidth} onChange={this.onChangeBorderRightWidth} />
          <Handle position="right" borderWidth={styles.borderRightWidth} onChange={this.onChangeBorderRightWidth} />
        </div>
        <div className="border-width-control__bottom">
          <ValueInput value={styles.borderBottomWidth} onChange={this.onChangeBorderBottomWidth} />
          <Handle position="bottom" borderWidth={styles.borderBottomWidth} onChange={this.onChangeBorderBottomWidth} />
        </div>
      </div>
    );
  }
}

BorderWidthControl.propTypes = {
  onChange: PropTypes.func.isRequired,
  styles: PropTypes.object.isRequired,
};
