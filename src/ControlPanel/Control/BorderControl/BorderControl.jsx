import React from 'react';
import PropTypes from 'prop-types';
import Dropdown from '../Dropdown/Dropdown';
import BorderControlMenu from './BorderControlMenu';
import BorderIcon from './BorderIcon';
import u from '../../../util';

export default class BorderControl extends React.Component {
  shouldComponentUpdate(nextProps) {
    return !u.shallowEquals(nextProps.styles, this.props.styles);
  }

  render() {
    const { styles, onChange } = this.props;
    return (
      <Dropdown overlay={<BorderControlMenu styles={styles} onChange={onChange} />}>
        <div className="toolbar-box">
          <div>
            <BorderIcon style={styles} />
          </div>
          <span>Border</span>
        </div>
      </Dropdown>
    );
  }
}

BorderControl.propTypes = {
  styles: PropTypes.shape({
    borderLeftWidth: PropTypes.number,
    borderRightWidth: PropTypes.number,
    borderTopWidth: PropTypes.number,
    borderBottomWidth: PropTypes.number,
    borderStyle: PropTypes.string,
    borderColor: PropTypes.string,
  }).isRequired,
  onChange: PropTypes.func.isRequired,
};
