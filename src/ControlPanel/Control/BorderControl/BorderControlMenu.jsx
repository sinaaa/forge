import React from 'react';
import PropTypes from 'prop-types';
import BorderWidthControl from './BorderWidthControl';
import BorderStyleControl from './BorderStyleControl';
import BorderColorControl from './BorderColorControl';
import './BorderControlMenu.css';

export default class BorderControlMenu extends React.Component {
  render() {
    const { styles, onChange } = this.props;
    return (
      <div role="menu" className="border-control-menu">
        <BorderWidthControl styles={styles} onChange={onChange} />
        <div className="border-control-menu__bottom">
          <BorderStyleControl
            borderStyle={styles.borderStyle}
            onChange={borderStyle => onChange({ borderStyle })}
          />
          <BorderColorControl
            borderColor={styles.borderColor}
            onChange={borderColor => onChange({ borderColor })}
          />
        </div>
      </div>
    );
  }
}

BorderControlMenu.propTypes = {
  styles: PropTypes.object.isRequired,
  onChange: PropTypes.func.isRequired,
};
