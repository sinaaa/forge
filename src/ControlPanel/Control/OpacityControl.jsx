import React from 'react';
import PropTypes from 'prop-types';
import Dropdown from './Dropdown/Dropdown';
import Menu from './Dropdown/Menu';

export default class OpacityControl extends React.Component {
  shouldComponentUpdate(nextProps) {
    return nextProps.opacity !== this.props.opacity;
  }

  render() {
    const { opacity, onChange } = this.props;
    return (
      <Dropdown
        overlay={<Menu
          items={[1, 0.9, 0.8, 0.7, 0.6, 0.5, 0.4, 0.3, 0.2, 0.1, 0]}
          selected={opacity}
          onChange={opacity => onChange({ opacity })}
        />}
      >
        <div className="toolbar-box">
          <div>
            <p>{opacity}</p>
          </div>
          <span>Opacity</span>
        </div>
      </Dropdown>
    );
  }
}

OpacityControl.propTypes = {
  opacity: PropTypes.number.isRequired,
  onChange: PropTypes.func.isRequired,
};
