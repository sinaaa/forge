import React from 'react';
import PropTypes from 'prop-types';
import FontSizeControl from './Control/FontSizeControl';
import BorderControl from './Control/BorderControl/BorderControl';
import RadiusControl from './Control/RadiusControl';
import OpacityControl from './Control/OpacityControl';
import ColorControl from './Control/ColorControl/ColorControl';
import ShadowControl from './Control/ShadowControl/ShadowControl';
import Tabs from './Tabs';
import u from '../util';
import './ControlPanel.css';

export default class ControlPanel extends React.Component {
  render() {
    const { styles, onChange, pseudoClass, onChangePseudoClass } = this.props;
    const borderStyles = u.pick(styles, [
      'borderLeftWidth',
      'borderRightWidth',
      'borderTopWidth',
      'borderBottomWidth',
      'borderStyle',
      'borderColor',
    ]);
    return (
      <header className="control-panel">
        <Tabs onChange={key => onChangePseudoClass(key)} activeKey={pseudoClass}>
          <button key="default">Default</button>
          <button key=":hover">Hover</button>
          <button key=":active">Active</button>
          <button key=":focus">Focus</button>
        </Tabs>
        <div className="toolbar" role="menubar">
          {'fontSize' in styles && <FontSizeControl size={styles.fontSize} onChange={onChange} />}
          {'borderRadius' in styles && <RadiusControl radius={styles.borderRadius} onChange={onChange} />}
          {'borderColor' in styles && <BorderControl styles={borderStyles} onChange={onChange} />}
          {'opacity' in styles && <OpacityControl opacity={styles.opacity} onChange={onChange} />}
          {'backgroundColor' in styles &&
            <ColorControl
              title="Background"
              color={styles.backgroundColor}
              onChange={backgroundColor => onChange({ backgroundColor })}
            />}
          {'color' in styles &&
            <ColorControl
              title="Color"
              color={styles.color}
              onChange={color => onChange({ color })}
            />}
          {'boxShadow' in styles && <ShadowControl boxShadow={styles.boxShadow} onChange={onChange} />}
        </div>
      </header>
    );
  }
}

ControlPanel.propTypes = {
  styles: PropTypes.object.isRequired,
  onChange: PropTypes.func.isRequired,
  pseudoClass: PropTypes.string,
  onChangePseudoClass: PropTypes.func.isRequired,
};

ControlPanel.defaultProps = {
  pseudoClass: null,
};
