import React from 'react';
import PropTypes from 'prop-types';
import './Tabs.css';

export default class Tabs extends React.Component {
  shouldComponentUpdate(nextProps) {
    return nextProps.activeKey !== this.props.activeKey;
  }

  render() {
    return (
      <div className="tablist">
        {React.Children.map(this.props.children, child =>
          React.cloneElement(child, {
            onClick: () => this.props.onChange(child.key),
            className: child.key === this.props.activeKey
              ? `${child.props.className} selected`
              : child.props.className,
          }))}
      </div>
    );
  }
}

Tabs.propTypes = {
  onChange: PropTypes.func.isRequired,
  activeKey: PropTypes.string.isRequired,
  children: PropTypes.any.isRequired,
};
