import React from 'react';
import PropTypes from 'prop-types';
import ButtonRow from './ButtonRow';
import './Main.css';

export default class Main extends React.Component {
  render() {
    const { onSelectComponent, styles } = this.props;
    return (
      <main className="main">
        <div>
          <ButtonRow
            label="Primary"
            onFocus={() => onSelectComponent('button')}
            style={styles.button}
          />
          <ButtonRow
            label="Secondary"
            onFocus={() => onSelectComponent('buttonSecondary')}
            style={styles.buttonSecondary}
          />
          <ButtonRow
            label="Disabled"
            onFocus={() => onSelectComponent('buttonDisabled')}
            style={styles.buttonDisabled}
          />
        </div>
      </main>
    );
  }
}

Main.propTypes = {
  onSelectComponent: PropTypes.func.isRequired,
  styles: PropTypes.object.isRequired,
};
