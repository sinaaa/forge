import React from 'react';
import PropTypes from 'prop-types';
import Radium from 'radium';
import u from '../util';
import './ButtonRow.css';

const EditIcon = ({ className }) => (
  <svg xmlns="http://www.w3.org/2000/svg" width="12" height="12" viewBox="0 0 512 512" className={className}>
    <path fill="currentColor" d="M163 439.573l-90.569-90.569L322.78 98.656l90.57 90.569zM471.723 88.393l-48.115-48.114c-11.723-11.724-31.558-10.896-44.304 1.85l-45.202 45.203 90.569 90.568 45.202-45.202c12.743-12.746 13.572-32.582 1.85-44.305zM64.021 363.252L32 480l116.737-32.021z" />
  </svg>
);

class ButtonRow extends React.Component {
  constructor(props) {
    super(props);
    this.handleMouseOut = this.handleMouseOut.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.state = {
      hovering: false,
      editing: false,
      value: 'Submit',
    };
  }

  handleMouseOut(e) {
    if (!this.labelEl.contains(e.relatedTarget)) {
      this.setState({ hovering: false });
    }
  }

  handleClick() {
    this.setState({ editing: !this.state.editing });
  }

  renderInput() {
    const { value } = this.state;
    const style = u.pick(this.props.style, ['color', 'fontSize', 'fontWeight']);
    return (
      <input
        ref={el => el && el.focus()}
        type="text"
        className="button-row__input"
        value={value}
        onChange={e => this.setState({ value: e.target.value })}
        onKeyDown={e => e.key === 'Enter' && this.setState({ editing: false })}
        onBlur={() => this.setState({ editing: false })}
        spellCheck={false}
        style={style}
      />
    );
  }

  render() {
    const { label, onFocus, style } = this.props;
    const { hovering, editing, value } = this.state;
    const iconClass = `edit-icon ${(hovering || editing) ? 'edit-icon--visible' : ''}`;
    return (
      <div className="button-row">
        <div className="button-row__label-wrap">
          <span
            className="button-row__label"
            ref={(el) => { this.labelEl = el; }}
            onMouseOver={() => this.setState({ hovering: true })}
            onMouseOut={this.handleMouseOut}
            onClick={this.handleClick}
          >
            {label}
            <EditIcon className={iconClass} />
          </span>
        </div>
        <button
          className="button-row__button"
          onFocus={onFocus}
          onMouseUp={e => e.preventDefault()}
          style={style}
        >
          {editing ? this.renderInput(value) : value}
        </button>
      </div>
    );
  }
}

ButtonRow.propTypes = {
  label: PropTypes.string.isRequired,
  onFocus: PropTypes.func.isRequired,
  style: PropTypes.object.isRequired,
};

export default Radium(ButtonRow);
