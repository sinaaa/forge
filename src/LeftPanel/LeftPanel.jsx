import React from 'react';
import './LeftPanel.css';

const PaintBucketIcon = () => (
  <svg width="32" height="32" viewBox="0 0 24 18" xmlns="http://www.w3.org/2000/svg" className="logo">
    <defs>
      <linearGradient id="g" x1="0" y1="0" x2="1" y2="0">
        <stop offset="0%" stopColor="#09203f" />
        <stop offset="100%" stopColor="#4e4376" />
      </linearGradient>
    </defs>
    <path d="M16.56 8.94L7.62 0 6.21 1.41l2.38 2.38-5.15 5.15c-.59.59-.59 1.54 0 2.12l5.5 5.5c.29.29.68.44 1.06.44s.77-.15 1.06-.44l5.5-5.5c.59-.58.59-1.53 0-2.12zM5.21 10L10 5.21 14.79 10H5.21zM19 11.5s-2 2.17-2 3.5c0 1.1.9 2 2 2s2-.9 2-2c0-1.33-2-3.5-2-3.5z" fillOpacity=".9" fill="url(#g)" />
  </svg>
);

export default class LeftPanel extends React.PureComponent {
  render() {
    return (
      <div className="left-panel">
        <div className="left-panel__toolbar">
          <PaintBucketIcon />
        </div>
        <div className="component-list">
          <button className="selected">Button</button>
        </div>
      </div>
    );
  }
}
