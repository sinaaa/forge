import React from 'react';
import PropTypes from 'prop-types';
import u from '../../util';
import './ValueInput.css';

export default class ValueInput extends React.Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.handleKeyDown = this.handleKeyDown.bind(this);
    this.handleWheel = this.handleWheel.bind(this);
    this.handleBlur = this.handleBlur.bind(this);
    this.initialValue = props.value;
    this.state = { value: props.value };
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.value !== nextProps.value) {
      this.initialValue = nextProps.value;
      this.setState({ value: nextProps.value });
    }
  }

  setValue(value) {
    let newValue = value;
    if (typeof this.props.value === 'number') {
      newValue = Number(value);
      if (isNaN(newValue)) {
        this.setState({ value: this.initialValue });
        return;
      }
      newValue = u.clamp(newValue, this.props.min, this.props.max);
    }
    this.initialValue = newValue;
    this.setState({ value: newValue });
    this.props.onChange(newValue);
  }

  isValid(v) {
    return !isNaN(v)
      && v >= this.props.min
      && v <= this.props.max;
  }

  handleChange(e) {
    this.setState({ value: e.target.value });
  }

  handleBlur(e) {
    this.setValue(e.target.value);
  }

  handleWheel(e) {
    if (typeof this.state.value === 'string') {
      return;
    }
    const value = Number(this.state.value);
    if (e.deltaY < 0) {
      if (this.isValid(value + 1)) {
        this.setValue(value + 1);
      }
    } else if (e.deltaY > 0) {
      if (this.isValid(value - 1)) {
        this.setValue(value - 1);
      }
    }
  }

  handleKeyDown(e) {
    if (typeof this.state.value === 'string') {
      return;
    }
    const value = Number(this.state.value);

    if (e.key === 'Enter') {
      e.target.blur();
      return;
    }
    if (e.key === 'ArrowUp') {
      e.preventDefault();
      if (this.isValid(value + 1)) {
        this.setValue(value + 1);
      }
      return;
    }
    if (e.key === 'ArrowDown') {
      e.preventDefault();
      if (this.isValid(value - 1)) {
        this.setValue(value - 1);
      }
    }
  }

  render() {
    return (
      <input
        type="text"
        id={this.props.id}
        spellCheck={false}
        className="value-input"
        style={this.props.style}
        value={this.state.value}
        onKeyDown={this.handleKeyDown}
        onChange={this.handleChange}
        onWheel={this.handleWheel}
        onBlur={this.handleBlur}
      />
    );
  }
}

ValueInput.propTypes = {
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  onChange: PropTypes.func.isRequired,
  min: PropTypes.number,
  max: PropTypes.number,
  style: PropTypes.object,
  id: PropTypes.string,
};

ValueInput.defaultProps = {
  min: 0,
  max: 20,
  style: null,
  id: null,
};
