import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import './Checkbox.css';

export default class Checkbox extends React.Component {
  render() {
    const { checked, onChange, id, disabled, style } = this.props;

    const checkboxClasses = cx('checkbox', {
      'checkbox--checked': checked,
      'checkbox--disabled': disabled,
    });

    return (
      <span
        className={checkboxClasses}
        style={{
          ...style,
          backgroundColor: checked ? style.backgroundColor : null,
          borderColor: disabled ? null : style.borderColor,
        }}
      >
        <input
          type="checkbox"
          id={id}
          className="checkbox__input"
          checked={checked}
          onChange={e => onChange(!!e.target.checked)}
          disabled={disabled}
        />
      </span>
    );
  }
}

Checkbox.propTypes = {
  checked: PropTypes.bool.isRequired,
  onChange: PropTypes.func.isRequired,
  disabled: PropTypes.bool,
  id: PropTypes.string,
  style: PropTypes.shape({
    backgroundColor: PropTypes.string,
    borderColor: PropTypes.string,
    borderRadius: PropTypes.number,
  }),
};

Checkbox.defaultProps = {
  disabled: false,
  style: {},
  id: null,
};
