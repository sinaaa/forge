import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import u from '../../util';
import './Slider.css';

export default class Slider extends React.Component {
  constructor(props) {
    super(props);
    this.handleKeyDown = this.handleKeyDown.bind(this);
    this.handleMouseDown = this.handleMouseDown.bind(this);
    this.handleMouseMove = this.handleMouseMove.bind(this);
    this.handleMouseUp = this.handleMouseUp.bind(this);
  }

  componentWillUnmount() {
    this.removeEventListeners();
  }

  handleKeyDown(ev) {
    const { disabled, onChange, value, step, min, max } = this.props;
    if (disabled) {
      return;
    }
    if (['ArrowRight', 'ArrowUp', 'Right', 'Up'].includes(ev.key)) {
      ev.stopPropagation();
      onChange(Math.min(value + (step || 1), max));
    } else if (['ArrowLeft', 'ArrowDown', 'Left', 'Down'].includes(ev.key)) {
      ev.stopPropagation();
      onChange(Math.max(value - (step || 1), min));
    }
  }

  handleMouseMove(ev) {
    ev.stopPropagation();
    this.updateValue(ev.pageX);
  }

  handleMouseDown(ev) {
    if (this.props.disabled || ev.button !== 0) {
      return;
    }
    ev.stopPropagation();
    this.addEventListeners();
    this.updateValue(ev.pageX);
  }

  handleMouseUp(ev) {
    if (ev.button !== 0) {
      return;
    }
    ev.stopPropagation();
    this.removeEventListeners();
  }

  updateValue(position) {
    if (!this.sliderEl) {
      return;
    }
    const { max, min } = this.props;
    const sliderStart = this.sliderEl.getBoundingClientRect().left;
    const offset = position - sliderStart;
    const ratio = Math.max(offset, 0) / this.sliderEl.clientWidth;
    const value = Math.round((ratio * (max - min)) + min);
    const newValue = this.roundToStep(value);

    if (newValue !== this.props.value) {
      this.props.onChange(newValue);
    }
  }

  roundToStep(value) {
    const { min, max, step } = this.props;
    if (value < min) return min;
    if (value > max) return max;
    if (!step) return value;
    const leftStep = Math.floor(value / step) * step;
    const rightStep = leftStep + step;
    const roundedValue = value - leftStep < rightStep - value
      ? leftStep
      : rightStep;
    return u.clamp(roundedValue, min, max);
  }

  addEventListeners() {
    document.addEventListener('mouseup', this.handleMouseUp);
    document.addEventListener('mousemove', this.handleMouseMove);
  }

  removeEventListeners() {
    document.removeEventListener('mouseup', this.handleMouseUp);
    document.removeEventListener('mousemove', this.handleMouseMove);
  }

  render() {
    const { value, min, max, disabled, style, className, ...other } = this.props;
    const fillWidth = `${((value - min) / (max - min)) * 100}%`;
    const sliderClasses = cx('slider', {
      'slider--disabled': disabled,
      [className]: className,
    });

    return (
      <div
        className={sliderClasses}
        role="presentation"
        style={style}
        onMouseDown={this.handleMouseDown}
        ref={(el) => { this.sliderEl = el; }}
        {...other}
      >
        <div className="slider__fill" style={{ width: fillWidth }} />
        <div
          className="slider__handle"
          tabIndex={0}
          role="slider"
          aria-valuemax={max}
          aria-valuemin={min}
          aria-valuenow={value}
          aria-disabled={disabled}
          onKeyDown={this.handleKeyDown}
          style={{ left: fillWidth }}
        />
        <div className="slider__empty" />
      </div>
    );
  }
}

Slider.propTypes = {
  onChange: PropTypes.func.isRequired,
  value: PropTypes.number.isRequired,
  min: PropTypes.number.isRequired,
  max: PropTypes.number.isRequired,
  step: PropTypes.number,
  disabled: PropTypes.bool,
  style: PropTypes.object,
  className: PropTypes.string,
};

Slider.defaultProps = {
  step: 0,
  disabled: false,
  style: null,
  className: '',
};
