import React from 'react';
import LeftPanel from './LeftPanel/LeftPanel';
import Main from './Main/Main';
import ControlPanel from './ControlPanel/ControlPanel';
import './App.css';

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.onControlChange = this.onControlChange.bind(this);
    const buttonStyles = {
      backgroundColor: '#f74b5f',
      color: '#fff',
      borderRadius: 2,
      fontSize: 14,
      borderTopWidth: 0,
      borderBottomWidth: 0,
      borderLeftWidth: 0,
      borderRightWidth: 0,
      borderStyle: 'solid',
      borderColor: '#4c90f7',
      opacity: 1,
      fontWeight: 700,
      boxShadow: '0 -2px rgba(0,0,0,.1) inset',
    };
    this.state = {
      selectedComponent: 'button',
      pseudoClass: 'default',
      button: {
        ...buttonStyles,
        ':hover': {
          opacity: 0.9,
        },
      },
      buttonSecondary: {
        ...buttonStyles,
        backgroundColor: '#dadada',
        borderColor: '#dadada',
        color: 'rgba(0,0,0,.4)',
        ':hover': {
          opacity: 0.8,
        },
      },
      buttonDisabled: {
        ...buttonStyles,
        backgroundColor: '#f2f2f2',
        borderColor: '#ccc',
        cursor: 'default',
        boxShadow: '0 -2px rgba(0,0,0,.05) inset',
      },
    };
  }

  onControlChange(stateChange) {
    const { pseudoClass, selectedComponent } = this.state;
    if (pseudoClass !== 'default') {
      this.setState({
        [selectedComponent]: {
          ...this.state[selectedComponent],
          [pseudoClass]: {
            ...this.state[selectedComponent][pseudoClass],
            ...stateChange,
          },
        },
      });
    } else {
      this.setState({
        [selectedComponent]: {
          ...this.state[selectedComponent],
          ...stateChange,
        },
      });
    }
  }

  getStyles(component) {
    return this.state.pseudoClass !== 'default' && this.state.selectedComponent === component
      ? {
        ...this.state[component],
        ...this.state[component][this.state.pseudoClass],
      }
      : this.state[component];
  }

  render() {
    return [
      <LeftPanel key="0" />,
      <ControlPanel
        key="1"
        styles={this.getStyles(this.state.selectedComponent)}
        onChange={this.onControlChange}
        pseudoClass={this.state.pseudoClass}
        onChangePseudoClass={pseudoClass => this.setState({ pseudoClass })}
      />,
      <Main
        key="2"
        onSelectComponent={selectedComponent => this.setState({ selectedComponent })}
        styles={Object.keys(this.state)
            .filter(key => key !== 'pseudoClass' && key !== 'selectedComponent')
            .reduce((acc, key) => ({ ...acc, [key]: this.getStyles(key) }), {})}
      />,
    ];
  }
}
